package demo.jsonlog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Démonstration log au format Json
 * 
 */
public class Main {
    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        LOG.info("Message simple: {}", "hello");
        MDC.put("Propriété 1", "Valeur 1");
        MDC.put("Propriété 2", "Valeur 2");
        LOG.info("Message avec propriétés");
        MDC.clear();
    }
}
