# Exemple de sortie de traces (log) au format Json

Cette micro-application illustre une sortie de traces (logs) au format Json.
Utile par exemple pour une exploitation ulétieure dans ELK

## Construction de l'application

Installer l'outil de constructon Apache Maven.
Le plugin Maven Exec est également installé pour simplifier les tests.

* mvn compile : compilation du projet
* mvn exec:java : exécute l'application (après compilation)

## Fonctionnement

La sortie au format Json n'est pas gérée par l'application mais par
simple configuration de la librairie de journalisation.

Cet exemple utilise Logback, le principe est identique avec Log4J2.

~ Fin
